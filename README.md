DESCRIPTION:
============
The Moon Phases module creates a block of the current moon phase,
as well as pages and calendars for given days or months.

INSTALLATION:
=============
1. Install as usual, see http://drupal.org/node/70151 for further information.
2. See configuration below for more information about the module configuration.

CONFIGURATION:
==============
There is limited configuration settiongs at Configuration -> Content authoring
-> Moon Phases, /admin/config/content/moon-phases. There you can alter the
default moon phase descriptions and choose to hide or show the image
attribution.

INCLUDED IMAGES:
================
This module include images from the NASA's Scientific Visualization Studio.

MAINTAINER:
===========
[tom.camp](https://www.drupal.org/u/tomcamp)
