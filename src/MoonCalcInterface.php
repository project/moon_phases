<?php

namespace Drupal\moon_phases;

use DateTime;

/**
 * Interface MoonCalcInterface.
 */
interface MoonCalcInterface {

  /**
   * Returns the moon phase name.
   *
   * @return string
   *   Returns the ID for a given moon name.
   */
  public function getMoonPhaseName();

  /**
   * Get the number of days until the next moon of a type; options include
   * Full Moon, First Quarter, Last Quarter and New Moon.
   *
   * @param string $type
   *   The moon phase.
   *
   * @return int
   *   Returns the number of days until the next full moon.
   */
  public function getDaysUntilNextMoonType($type);

  /**
   * Get the percentage of illumination for the moon phase.
   *
   * @return float
   *   Returns the percentage of illumination.
   */
  public function getPercentOfIllumination();

  /**
   * Returns the moon phase date as a Unix timestamp.
   *
   * @return int
   *   Returns the date as a Unix timestamp.
   */
  public function getMoonPhaseDate();

  /**
   * Returns the moon phase date as a DateTime object.
   *
   * @return \Datetime
   *   Returns the date as a DateTime object.
   */
  public function getMoonPhaseDateTime();

  /**
   * Creates a path to the moon phase image.
   *
   * @return string
   *   Returns the URI for moon phase image.
   */
  public function getImageUri();

}
