<?php

namespace Drupal\moon_phases;

use DateTime;

/**
 * {@inheritdoc}
 */
class MoonCalc implements MoonCalcInterface {

  /**
   * Set the number of days in a moon phase.
   *
   * @var float
   */
  protected $periodInDays = 29.53058867;

  /**
   * Set the number of seconds in a moon phase; $periodInDays * 86400.
   *
   * @var float
   */
  protected $periodInSeconds = 2551442.86108;

  /**
   * The position, between 0 and 1, of the moon phase.
   *
   * @var float
   */
  protected $positionInCycle;

  /**
   * The ID for the phase for a given date.
   *
   * @var int
   */
  protected $phaseId;

  /**
   * The percentage of illumination for the moon phase.
   *
   * @var float
   */
  protected $percentOfIllumination;

  /**
   * The date for which to calculate the moon phase.
   *
   * @var \DateTime
   */
  protected $phaseDate;

  /**
   * An array containing the names of the phases, keyed by the phase ID.
   *
   * @var array
   */
  protected $phaseName = [
    0 => 'New Moon',
    1 => 'Waxing Crescent',
    2 => 'First Quarter',
    3 => 'Waxing Gibbous',
    4 => 'Full Moon',
    5 => 'Waning Gibbous',
    6 => 'Last Quarter',
    7 => 'Waning Crescent',
  ];

  /**
   * MoonCalc constructor.
   *
   * @param \DateTime $date
   *   The date as DateTime.
   */
  public function __construct(DateTime $date) {
    $this->phaseDate = $date;
    $this->setPositionInCycle($date);
    $this->calcMoonPhase();
  }

  /**
   * Sets the position in the current phase cycle.
   */
  private function setPositionInCycle() {
    $diff = $this->getMoonPhaseDate() - $this->getBaseFullMoonDate();
    $position = ($diff % $this->periodInSeconds) / $this->periodInSeconds;
    if ($position < 0) {
      $position = 1 + $position;
    }
    $this->positionInCycle = $position;
  }

  /**
   * Sets the moon phase ID and moon phase name.
   */
  private function calcMoonPhase() {
    $position = $this->positionInCycle;

    if ($position >= 0.474 && $position <= 0.53) {
      $this->phaseId = 0;
    }
    elseif ($position >= 0.54 && $position <= 0.724) {
      $this->phaseId = 1;
    }
    elseif ($position >= 0.725 && $position <= 0.776) {
      $this->phaseId = 2;
    }
    elseif ($position >= 0.777 && $position <= 0.974) {
      $this->phaseId = 3;
    }
    elseif ($position >= 0.975 || $position <= 0.026) {
      $this->phaseId = 4;
    }
    elseif ($position >= 0.027 && $position <= 0.234) {
      $this->phaseId = 5;
    }
    elseif ($position >= 0.235 && $position <= 0.295) {
      $this->phaseId = 6;
    }
    else {
      $this->phaseId =7;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMoonPhaseName() {
    return $this->phaseName[$this->phaseId];
  }

  public function getDaysUntilNextMoonType($type = 'Full Moon') {
    switch ($type) {
      case 'Last Quarter':
        $delimiter = 0.25;
        break;

      case 'New Moon':
        $delimiter = 0.5;
        break;

      case 'First Quarter':
        $delimiter = 0.75;
        break;

      case 'Full Moon':
        $delimiter = 1;
        break;

      default:
        $message = t('Cannot display days until %type', ['%type' => $type]);
        $messenger = \Drupal::messenger();
        $messenger->addError($message, FALSE);
        return;
    }
    return $this->getDaysUntil($delimiter);
  }

  private function getDaysUntil($delimiter) {
    $precision = ($delimiter === 1) ? 2 : 1;
    $days = 0;
    $position = $this->positionInCycle;
    if ($position < $delimiter) {
      $days = ($delimiter - $position) * $this->periodInDays;
    }
    elseif ($position >= $delimiter) {
      $days = (($delimiter + 1) - $position) * $this->periodInDays;
    }
    return round($days, $precision);
  }

  /**
   * {@inheritdoc}
   */
  public function getPercentOfIllumination() {
    $percentage = (1.0 + cos(2.0 * M_PI * $this->positionInCycle)) / 2.0;
    $percentage *= 100;
    return round($percentage, 2);
  }

  /**
   * {@inheritdoc}
   */
  public function getMoonPhaseDate() {
    return strtotime($this->phaseDate->format('Y-m-d H:i:s'));
  }

  public function getMoonPhaseDateTime() {
    return $this->phaseDate;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageUri() {
    $id = $this->positionInCycle;
    $phase_id = number_format($id, 2) * 100;
    $phase_id = ($phase_id > 99) ? 00 : $phase_id;
    $path = drupal_get_path('module', 'moon_phases') . '/images/';
    return $path . 'moon.' . str_pad($phase_id, 2, 0, STR_PAD_LEFT) . '.png';
  }

  /**
   * Returns base date to a known full moon date.
   */
  private function getBaseFullMoonDate() {
    // (http://aa.usno.navy.mil/data/docs/MoonPhase.html)
    return strtotime('December 12 2008 16:37 UTC');
  }

}
